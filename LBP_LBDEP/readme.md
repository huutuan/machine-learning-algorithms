# This project is an implementation of the [paper](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8856196) to solve *Defect Classification* problem

# To run
python main.py --impath path/to/image

