import os
import cv2
import glob

from matplotlib import pyplot as plt
import numpy as np
from utils import *

folder = '/home/tuanlh/working/CrackDetection/io'
template = folder + '/*output.*'
outdir = '/home/tuanlh/working/CrackDetection/out'

for out_path in glob.glob(template):
    img_name = out_path.split('/')[-1].replace('_output', '')
    image_org_path = out_path.replace('_output', '')
    image_org = cv2.imread(image_org_path)

    image_out = cv2.imread(out_path)
    print('> size : ', image_org.shape, image_out.shape)

    sub_image = image_out.astype(np.float32) - image_org.astype(np.float32)
    # sub_image = image_out - image_org
    # sub_image = np.where(sub_image < 0, 0, sub_image)
    # sub_image = np.where(sub_image > 255, 255, sub_image)
    
    sub_image = sub_image.astype(np.int32)
    print('sub: ', sub_image.shape)


    lbp = LocalBinaryPatterns()
    lbp_img = lbp(sub_image.astype(np.float32))
    de = DE()
    de_img = de(sub_image.astype(np.float32))
    de_img = 255 - de_img
    # de_img = np.where(de_img > 0.8, de_img, 0)


    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(1,5, figsize=(10, 3))
    ax1.imshow(image_org, cmap='gray')
    ax1.set_title('Origin Image')

    ax2.imshow(image_out, cmap='gray')
    ax2.set_title('Output Image')

    # ax3.imshow(sub_image, cmap='gray')
    ax3.imshow(sub_image, cmap='gray')
    ax3.set_title('Sub Image')
    
    # ax3.imshow(sub_image, cmap='gray')
    ax4.imshow(lbp_img, cmap='gray')
    ax4.set_title('LBP Image')
    
    # ax3.imshow(sub_image, cmap='gray')
    ax5.imshow(de_img, cmap='gray')
    ax5.set_title('DE Image')

    plt.show()
    assert 0
    # plt.savefig(os.path.join(folder, 'out', img_name))