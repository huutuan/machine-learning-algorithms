import os
import cv2
import argparse
import math
import glob

import skimage
from skimage import feature
import numpy as np
from matplotlib import pyplot as plt

from utils import LocalBinaryPatterns, DE

imsize = (128, 128)


def parse_args():
    parser = argparse.ArgumentParser(description='Run demo')
    parser.add_argument('--impath', type=str)
    args = parser.parse_args()

    return args

def normalize(arr, t_min, t_max):
    norm_arr = []
    diff = t_max - t_min
    diff_arr = max(arr) - min(arr)    
    for i in arr:
        temp = (((i - min(arr))*diff)/diff_arr) + t_min
        norm_arr.append(temp)
    return norm_arr
  
def feature_2dgeneration(h1, h2):
    """
        h1: histogram of LBP
        h2: histogram of LB_DEP
        len(h1) = len(h2) = L 
    """
    a1, b1 = min(h1), max(h1)
    a2, b2 = min(h2), max(h2)
    w = b1 - a1
    h = b2 - a2
    feat_2d = np.zeros((w,h))
    length = len(h1)

    for i, y1 in enumerate(range(a1, b1+1)):
        h1_x = [x for x in range(length) if h1[x] == y1]        # find all x corrdidate if h1[x] == y1
        for j, y2 in enumerate(range(a2, b2+1)):
            nij = len([x for x in h1_x if h2[x] == y2])          # number of pair (i, j) if [h1[]] 
            feat_2d[i, j] = nij

    return feat_2d

def feature_1dgeneration(h1, h2):
    """
        h1: histogram of LBP
        h2: histogram of LB_DEP
        len(h1) = len(h2) = L 
    """

    return

def normalize_image(img, mode = 'lbp'):
    """
        if LPB image, normalize in [0,1]
        if DE image, normalize in [-1,1]

    """
    if mode == 'lbp':
        range_to_normalize = (0,1)
    elif mode == 'lbdep':
        range_to_normalize = (-1,1)
    else:
        assert 0, 'mode must be lbp or lbdep'
    normal_image = normalize(img, range_to_normalize[0], range_to_normalize[1])

    return normal_image

def euclidean_distance(G):
    """
        We know Gc(G crack) and Gn(G normal).
        => E(G, Gc) & E(G, Gn)
        Return: 
            If d_Gc <= d_Gn >>> return False (Crack)
    """
    d_Gc = np.sqrt(np.sum((G-Gc)*2))
    d_Gn = np.sqrt(np.sum((G-Gn)*2))
    if d_Gc > d_Gn:
        return True
    else:
        return False

def calculate_sample(dir):
    if len(glob.glob(dir)) > 100:
        assert 0, 'warning!!!'
    lbp = LocalBinaryPatterns()
    de = DE()

    feat_2ds = []
    for path in glob.glob(dir):
        img = cv2.imread(path)
        img = cv2.resize(img, imsize, interpolation = cv2.INTER_AREA)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        lbp_img = lbp(img)

        lbp_hist, _ = np.histogram(lbp_img.ravel(), bins= 256, range = (0, 256))
        de_img = de(img)
        de_hist = de.calculate_ft(lbp_img)

        feat_2d = feature_2dgeneration(h1, h2)
        feat_2ds.append(feat_2d)

    feat_2ds = np.stack(feat_2ds, axis=0)
    return np.mean(feat_2ds, axis=0)

def main(path):
    imgname = os.path.basename(path)
    img = cv2.imread(path)
    img = cv2.resize(img, imsize, interpolation = cv2.INTER_AREA)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    lbp = LocalBinaryPatterns()
    lbp_img = lbp(img)

    """ plot LBP histogram """
    # plt.title("Histogram of Crack")
    # plt.xlabel("Value")
    # plt.ylabel("Pixels Frequency")
    # plt.hist(lbp_img)
    # # plt.show()
    
    de = DE()
    de_img = de(img)
    de_hist = de.calculate_ft(lbp_img)
    # print('de shape: ', de_img.shape, de_img)
    # print('de hist: ', len(de_hist), de_hist)

    """ plot DE histogram """
    # X = np.arange(256)
    # fig = plt.figure()
    # ax = fig.add_axes([0,0,1,1])
    # ax.bar(X, de_hist, color = 'b', width = 0.01)
    # plt.show()

    fill = np.where(de_img < 1, 1, 0)
    
    # plot stack image
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(10, 3))
    ax1.imshow(img, cmap='gray')
    ax1.set_title('Origin Image')
    ax2.imshow(lbp_img, cmap='gray')
    ax2.set_title('LBP Image')
    ax3.imshow(de_img, cmap='gray')
    ax3.set_title('DE Image')
    ax4.imshow(fill, cmap='gray')
    ax4.set_title('DE transform')
    
    plt.show()
    # plt.savefig('path/to/save')

    return

if __name__ == '__main__':
    # defect_dir = '/home/tuanlh/working/CrackDetection/src/LBP_LBDEP/sample/crack'
    # gc = calculate_sample(defect_dir)
    # defect_dir = '/home/tuanlh/working/CrackDetection/src/LBP_LBDEP/sample/non-crack'
    # gn = calculate_sample(defect_dir)
    
    args = parse_args()
    impath = args.impath
    main(impath)