import os
import cv2
import argparse
import math

import skimage
from skimage import feature
import numpy as np
from matplotlib import pyplot as plt

__all__ = ['LocalBinaryPatterns', 'DE']

def padding(array):
    m, n = array.shape
    pad_array = np.zeros((m+2,n+2))

    pad_array[1:m+1, 1:n+1] = array             # center

    pad_array[1:m+1, 0] = array[:,0]            # left
    pad_array[1:m+1, n+1] = array[:,n-1]        # right
    pad_array[0, 1:n+1] = array[0,:]            # top
    pad_array[m+1, 1:n+1] = array[m-1,:]        # bottom

    pad_array[0, 0] = array[0,0]                # tl
    pad_array[m+1, 0] = array[m-1,0]            # bl
    pad_array[m+1, n+1] = array[m-1,n-1]        # br
    pad_array[0, n+1] = array[0,n-1]            # tr
    pad_array = pad_array.astype(np.int)
    # print('pad: ', pad_array)
    
    return pad_array


class LocalBinaryPatterns():
    def __init__(self, radius = 1, texture = '8'):
        self.type = texture             # 8TYPE: 4X, 4+, 2|, 2-, 2/, 2\, 6=, 8
        self.radius = radius

    def __call__(self, image):
        if len(image.shape) > 2:
            image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        pad_img = padding(image)

        m, n = pad_img.shape
        lbp_image = np.zeros((m-2, n-2))

        for i in range(m-2):
            for j in range(n-2):
                area = pad_img[i:i+3, j:j+3]    # shape = 3x3
                # print('area: ', area.shape)
                val = self.cal_lpb(area)
                lbp_image[i,j] = val
        
        return lbp_image

    def cal_lpb(self, area):
        lbp_arr = np.where(area >= area[1,1], 1, 0)

        # reference: https://medium.com/swlh/local-binary-pattern-algorithm-the-math-behind-it-%EF%B8%8F-edf7b0e1c8b3
        if self.type == '8':    
            index = [[1,2], [0,2], [0,1], [0,0], [1,0], [2,0], [2,1], [2,2]]

        binary = [lbp_arr[m,n] for (m,n) in index]
        value = sum([val*2**idx for idx, val in enumerate(binary)])
        # print(value)
        return value


class DE():
    """ differential excitation """
    def __init__(self, alpha = 5):
        self.alpha = alpha
        self.type = '8'
        if self.type == '8':
            self.sample_points = 8
        self.indexs = [[-1,-1], [-1,0], [-1,1], [0,-1], [0,1], [1,-1], [1,0], [1,1]]
        self.de_img = None

    def __call__(self, image):
        if len(image.shape) > 2:
            image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        h, w = image.shape
        de_array = np.zeros((h, w))

        for x in range(w):
            for y in range(h):
                value = self.cal_de_value((x,y), image)
                de_array[x,y] = value

        # de_array = self.rescale(de_array).astype(np.int)
        self.de_img = de_array
        return de_array
        
    def rescale(self, image):
        w, h = image.shape
        reimage = np.zeros((w,h))
        for x in range(w):
            for y in range(h):
                reimage[x,y] = int((image[x,y] + math.pi/2)*255/math.pi)
        return reimage
    
    def cal_de_value(self, index, image):
        image = image.astype(np.int)
        center_value = image[index[0], index[1]]
        total = 0.0
        for sub in self.indexs:
            try:
                neigh_index = [index[0] + sub[0], index[1] + sub[1]]
                neigh_value = image[neigh_index[0], neigh_index[1]]
                total += (neigh_value - center_value)
            except:
                # print('position: ', x,y)
                continue
        # print('total: ', total)
        out = (self.alpha * total)/(np.max(image) - np.min(image))
        acrtan = math.atan(out)
        return acrtan

    def calculate_ft(self, lbp_img):
        """ calculate F(t) """
        w, h = lbp_img.shape
        lbp_img = lbp_img.astype(np.int)
        ft = []
        for t in range(2**self.sample_points):
            Ib = np.zeros((w,h)).astype(np.int)
            for x in range(w):
                for y in range(h):
                    if lbp_img[x, y] == t:
                        Ib[x,y] = 1
                    else:
                        Ib[x,y] = 0
            # print('DE img', np.max(self.de_img), np.min(self.de_img), self.de_img)
            # print('Ib: ', Ib)
            de_mapping = np.multiply(self.de_img, Ib).astype(np.int)
            # print('DE mapping: ', de_mapping)
            # print(np.sum(de_mapping))
            # assert 0     
            ft.append(np.sum(de_mapping))
        return ft
    