
import numpy as np 
# import tensorflow as tf 


def softmax(Z):
	"""
		Do tính e^x có thể gây tràn số nên ta có cách tính khác của softmax là
		thay zi = zi - C trong mọi tính toán (với C là giá trị max trong Z)
        (The other way to compute the softmax function, put zi = zi -c. 
        the result are similarity)
	"""
	e_Z = np.exp(Z - np.max(Z, axis = 0, keepdims = True))
	# keepdims đảm bảo phép trừ có thể thực hiện được
	return e_Z/np.sum(e_Z, axis=1)
    # return np.exp(z) / np.sum(np.exp(z), axis=1).reshape(z.shape[0], 1)

def rnn_cell(xt, h_prev, parameters):
	"""
		input:
			xt - input data for every timestep, shape (n_x, m)
			h0 - initial hidden state (first block), shape (n_a, m)
		parameters:  
			Wx - weight matrix of the input (xt), shape (n_a, n_x)
			Wh - weight matrix of the hidden state (h_prev), shape (n_a, n_a)
			Wy - weight matrix of the output (hidden state ->> output), shape (n_y, n_a)
			bxh - bias , f0 = Wx*xt + Wh*h_prev + bxh
			by - bias , f1 = Wy*f0 + by
	"""
	Wx = parameters['Wx']
	Wh = parameters['Wh']
	Wy = parameters['Wy']
	bxh = parameters['bxh']
	by = parameters['by']

	print('x,h shape--', xt.shape, h_prev.shape)

	# compute the new hidden
	h_next = np.dot(Wx, xt) + np.dot(Wh, h_prev) + bxh	
	# apply tanh function
	h_next = np.tanh(h_next)

	# compute the output
	y_pred = np.dot(Wy, h_next) + by
	print('y_pred shape ---', y_pred.shape)
	# apply softmax function
	y_pred = softmax(y_pred)

	cache = (h_next, h_prev, xt, parameters)
	return h_next, y_pred, cache

def rnn_cell_backward():

	return []



class RNN():
	def __init__(self):
		self.layer = None
		self.dim = None 

	def forward(self, x, h0, parameters):
		"""
			input:
				xt - input data for every timestep, shape (d_x, m, T_X)  		>@dim_x, T_X is length of string x
				h0 - initial hidden state (first block), shape (d_h, m)			>@dim_hidden, 
			parameters: 
					Wx - weight matrix of the input (xt), shape (d_h, d_x)
					Wh - weight matrix of the hidden state (h_prev), shape (d_h, d_h)
					Wy - weight matrix of the output (hidden state ->> output), shape (d_y, d_h)	 >@dim_y
					bxh - bias , f0 = Wx*xt + Wh*h_prev + bxh
					by - bias , f1 = Wy*f0 + by
			caculate:
				h_pred : shape (d_h, m)
				y_pred : shape (d_y, m)
	"""
		
		d_x, m, T_X = x.shape
		d_y, d_h = parameters["Wy"].shape

		h = np.zeros((d_x, m, T_X))
		y_pred = np.zeros((d_y, m, T_X))

		caches = []
		h_next = h0

		for t in range(T_X):
			h_next, yt_pred, cache = rnn_cell(x[:,:,t], h_next, parameters)
			## save
			h[:,:,t] = h_next		# append weight at step t
			y_pred[:,:,t] = yt_pred		# append y_pred at step t
			caches.append(cache)
		return h, y_pred, caches


### >> 1 iteration (1 timestep), one word
def check_rnn_cell():
	# initization 
	# dim(wx*xt) = dim(wh*ht) = (5,10)  = dim(h_next)
	# dim(wy*h_next) = (2,10) 

	xt = np.random.randn(5,10)
	ht = np.random.randn(3,10)
	Wx = np.random.randn(5,5)
	Wh = np.random.randn(5,3)	
	Wy = np.random.randn(1,5)

	bxh = np.random.randn(5,1)	# ~h_next 
	by = np.random.randn(1,1)	# ~wy

	parameters = {'Wx':Wx, 
					'Wh':Wh,
					'Wy':Wy,
					'bxh':bxh,
					'by':by
					}

	h, y, cache = rnn_cell(xt,ht,parameters)
	print('h-next ----\n',h)
	print('y-next ----\n',y)


### >> n interation , n word, save sequence of weight, no training. 
def check_rnn_forward():
	np.random.seed(1)
	x = np.random.randn(5,10,5)
	h0 = np.random.randn(5,10)
	Wx = np.random.randn(5,5)
	Wh = np.random.randn(5,5)	
	Wy = np.random.randn(1,5)
	bxh = np.random.randn(5,1)	# ~h_next 
	by = np.random.randn(1,1)	# ~wy

	parameters = {'Wx':Wx, 
					'Wh':Wh,
					'Wy':Wy,
					'bxh':bxh,
					'by':by
					}

	rnn = RNN()
	h, y_pred, caches = rnn.forward(x, h0, parameters)
	print("\nh.shape = ", h.shape)
	print("y_pred.shape = ", y_pred.shape)
	print("h[4][1] = ", h[4][1])
	print("y_pred[1][3] =", y_pred[0][3])
	print("caches[1][1][3] =", caches[1][1][3])
	print("len(caches) = ", len(caches))


check_rnn_cell()
# check_rnn_forward()

