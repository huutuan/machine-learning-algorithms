
import numpy as np 
# import tensorflow as tf 


def sigmoid(S):
	return 1/(1 + np.exp(-S))

def softmax(Z):
	"""
		Do tính e^x có thể gây tràn số nên ta có cách tính khác của softmax là
		thay zi = zi - C trong mọi tính toán (với C là giá trị max trong Z)
        (The other way to compute the softmax function, put zi = zi -c. 
        the result are similarity)
	"""
	e_Z = np.exp(Z - np.max(Z, axis = 0, keepdims = True))
	# keepdims đảm bảo phép trừ có thể thực hiện được
	return e_Z/np.sum(e_Z, axis=1)


def lstm_cell(xt, h_prev, c_prev, parameters):
    """
        input: 
            xt - input data at timestep "t", numpy array of shape (n_x, m).
            h_prev - Hidden state at timestep "t-1", numpy array of shape (n_a, m)
            c_prev - Memory state at timestep "t-1", numpy array of shape (n_a, m)
        parameters:
            Wf   - weight matrix of the forget gate, shape (d_h, d_h + d_x)
            bf   - bias of the fotget gate, shape (d_h, 1)
            Wi   - weight matrix of the update gate, shape (d_h, d_h + d_x)
            bi   - bias of the update gate, shape (d_h, 1)
            Wc   - weight matrix of the first 'tanh' func, shape (d_h, d_h + d_x)
            bc   - bias with Wc, shape (d_h, 1)
            Wo   - weight matrix of the output gate, shape (d_h, d_h + d_x) 
            bo   - bias with Wo, shape (d_h, 1)
            Wy   - weight matrix of the hidden-state to the output, shape (d_y, d_h) 
            by   - bias with Wc, shape (d_y, 1)
        output:

    """
    Wf = parameters["Wf"]
    bf = parameters["bf"]
    Wi = parameters["Wi"]
    bi = parameters["bi"]
    Wc = parameters["Wc"]
    bc = parameters["bc"]
    Wo = parameters["Wo"]
    bo = parameters["bo"]
    Wy = parameters["Wy"]
    by = parameters["by"]

    d_x, m = xt.shape
    d_y, d_h = Wy.shape

    # concat flow collum h_prev vs xt
    concat = np.concatenate((h_prev, xt), axis=0)


    # Γ⟨t⟩f=σ(Wf[a⟨t−1⟩,x⟨t⟩]+bf)
    ft = sigmoid(np.dot(Wf,concat)+bf)
    # Γ⟨t⟩u=σ(Wu[a⟨t−1⟩,x{t}]+bu)
    it = sigmoid(np.dot(Wi,concat)+bi)
    # c̃ ⟨t⟩=tanh(Wc[a⟨t−1⟩,x⟨t⟩]+bc)
    cct = np.tanh(np.dot(Wc,concat)+bc)
    # c⟨t⟩=Γ⟨t⟩f∗c⟨t−1⟩+Γ⟨t⟩u∗c̃ ⟨t⟩
    c_next = np.multiply(ft,c_prev)+ np.multiply(it,cct)
    # Γ⟨t⟩o=σ(Wo[a⟨t−1⟩,x⟨t⟩]+bo)
    ot = sigmoid(np.dot(Wo,concat)+bo)
    # a⟨t⟩=Γ⟨t⟩o∗tanh(c⟨t⟩)
    h_next = ot*np.tanh(c_next)
    
    # Compute prediction of the LSTM cell (≈1 line)
    yt_pred = softmax(np.dot(Wy, h_next) + by)
    ### END CODE HERE ###

    # store values needed for backward propagation in cache
    cache = (h_next, c_next, h_prev, c_prev, ft, it, cct, ot, xt, parameters)

    return h_next, c_next, yt_pred, cache

class LSTM():
    def __init__(self):
        self.dim = None
    
    def forward(self, x, h0, parameters):
        """
        callback: lstm_cell 
        """

        caches = []
        d_x, m, T_X = x.shape
        d_y, d_a = parameters["Wy"].shape

        h = np.zeros((d_a, m, T_X))
        c = np.zeros((d_a, m, T_X))
        y = np.zeros((d_y, m, T_X))

        h_next = h0
        c_next = np.zeros((d_a, m))

        for t in range(T_X):
            h_next, c_next, yt_pred, cache = lstm_cell(x[:,:,t], h_next, c_next, parameters)
            ## save
            h[:,:,t] = h_next
            y[:,:,t] = yt_pred
            c[:,:,t] = c_next
            caches.append(cache)
        caches = (caches, x)
        return h, y, c, caches



def check_lstm_cell():
    np.random.seed(1)
    xt = np.random.randn(3,10)
    a_prev = np.random.randn(5,10)
    c_prev = np.random.randn(5,10)
    Wf = np.random.randn(5, 5+3)
    bf = np.random.randn(5,1)
    Wi = np.random.randn(5, 5+3)
    bi = np.random.randn(5,1)
    Wo = np.random.randn(5, 5+3)
    bo = np.random.randn(5,1)
    Wc = np.random.randn(5, 5+3)
    bc = np.random.randn(5,1)
    Wy = np.random.randn(1,5)
    by = np.random.randn(1,1)

    parameters = {"Wf": Wf, 
                    "Wi": Wi, 
                    "Wo": Wo, 
                    "Wc": Wc, 
                    "Wy": Wy, 
                    "bf": bf, 
                    "bi": bi, 
                    "bo": bo, 
                    "bc": bc, 
                    "by": by}
    
    a_next, c_next, yt, cache = lstm_cell(xt, a_prev, c_prev, parameters)
    print('shape---', a_next.shape, c_next.shape, yt.shape)


def check_lstm_forward():
    np.random.seed(1)
    x = np.random.randn(3,10,7)
    h0 = np.random.randn(5,10)
    Wf = np.random.randn(5, 5+3)
    bf = np.random.randn(5,1)
    Wi = np.random.randn(5, 5+3)
    bi = np.random.randn(5,1)
    Wo = np.random.randn(5, 5+3)
    bo = np.random.randn(5,1)
    Wc = np.random.randn(5, 5+3)
    bc = np.random.randn(5,1)
    Wy = np.random.randn(1,5)
    by = np.random.randn(1,1)

    parameters = {"Wf": Wf, 
                    "Wi": Wi, 
                    "Wo": Wo, 
                    "Wc": Wc, 
                    "Wy": Wy, 
                    "bf": bf, 
                    "bi": bi, 
                    "bo": bo, 
                    "bc": bc, 
                    "by": by}
    
    lstm = LSTM()
    h, y, c, caches = lstm.forward(x, h0, parameters)
    
    print('shape---',h.shape, y.shape, c.shape)


# check_lstm_cell()
# check_lstm_forward()